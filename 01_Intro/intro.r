#set working directory from getwd plus /01_Intro
setwd(paste(getwd(),"/01_Intro",sep=""))
getwd()

#read urban.txt as matrix
urban = as.matrix (read.table ("Urban.txt", header=T))
save(urban, file="urban.RData")
specs <- urban


#Q1.1: Are the wavelength given in nm or in µm? Which wavelength range is covered? Are the reflectance values scaled [0,1] or in [0,100]%?



#prepare a spectral library
wl <- seq (400, 2445, 5) ## row of numbers from 400 to 2445 in intervals of 5
id <- rownames (specs) ## to identify each spectrum
speclib <- list (wl, specs, id) ## combine the spectra and wavelength info to a
## spectral library
names (speclib) <- c("wl", "specs", "id") ## add labels to the speclib elements


## plot the  the spectral library
#library(graphics)
plot (wl, specs[1,], main=id[1], ylim=c (0, 1), type="l",xlab="wavelength / nm", ylab="reflectance") # or

plot (speclib$wl, speclib$specs[1,], main=speclib$id[1], ylim=c (0, 1), type="l", xlab="wavelength / nm", ylab="reflectance")


##in the human perception, the color blue corresponds roughly to 450 nm, green to 550 nm, and red to 630 nm
abline (v=450, col="blue") ## add a blue vertical line at 450 nm
abline (v=550, col="green") ## add a green vertical line at 450 nm
abline (v=630, col="red") ## add a red vertical line at 450 nm


##Q 1.2: What color would the measured object have if you would be seeing it in reality?









## plot all spectra in one plot
plot (speclib$wl, speclib$specs[1,], ylim=c (0, 1.2), lwd=2, , type="l",
xlab="wavelength / nm", ylab="reflectance")
lines (speclib$wl, speclib$specs[2,], col="red", lwd=2) ## add the 2nd
lines (speclib$wl, speclib$specs[3,], col="green", lwd=2)
lines (speclib$wl, speclib$specs[4,], col="blue", lwd=2)
lines (speclib$wl, speclib$specs[5,], col="cyan", lwd=2)
lines (speclib$wl, speclib$specs[6,], col="magenta", lwd=2)
lines (speclib$wl, speclib$specs[7,], col="orange", lwd=2)
lines (speclib$wl, speclib$specs[8,], col="purple", lwd=2)
lines (speclib$wl, speclib$specs[9,], col="gray", lwd=2)
lines (speclib$wl, speclib$specs[10,], col="steelblue", lwd=2)


legend ("topright", legend=speclib$id, col=c ("black", "red",
"green", "blue", "cyan", "magenta", "orange", "purple", "gray",
"steelblue"), lwd=2, ncol=2, cex=0.7)

#Q1.3: Compare the spectra of the different objects regarding their shape and magnitude. Which
#characteristic (dis­)similarities can be observed? Urban: take a look at spectra belonging to similar materials
#(asphalt, vegetation, etc.). Vegetation: take into consideration the different leaf traits (needleleaf, broadleaf)
#and water content (dry vegetation). Minerals: can you identify characteristic features in the SWIR region?






#visualizing the inter­correlation of spectral bands
cm <- cor (speclib$specs) ## calculate the correlation matrix
cm[row (cm) > col (cm)] <- NA ## eliminate the upper half

#the terra­package in R enables the conversion of data tables (matrices) into gridded raster data for further analysis
#and visualization and makes the GDAL . We will use it later for handling image data sets and explore this functionality
#for the visualization of the correlation matrix, too

install.packages ("terra") ## only before first­time use if not yet installed
library(terra) ## load package
jet.colors <- colorRampPalette (c ("#00007F", "blue", "#007FFF", "cyan",
"#7FFF7F", "yellow", "#FF7F00", "red", "#7F0000"))

## create a color gradient
correlog <- rast (cm[410:1,], extent=ext(400, 2445,400, 2445))
## convert cm in a raster data set
plot (correlog, col=jet.colors (100), main="Correlation R",
xlab="wavelength / nm", ylab="wavelength / nm")

#Q1.4: Describe the inter­correlation of spectral bands in your data set. Can you identify distinct regions with
#a similar spectral behavior (R>0.7)?









#Q1.5: If you were a engineer and asked to design a multispectral sensor with five bands for urban /
#vegetation / geology & soil mapping, where would you place the bands based on the observed intercorrelation patterns?






#image data
geo.im <- rast ("0614-1124_ref.dat") ## read the image as raster data
geo.wl <- as.vector (t (read.table ("0614-1124_wl.txt")) * 1000) ## read the
## wavelengths in nm
veg.im <- rast ("Suwannee_0609-1331_ref.dat")
veg.wl <- as.vector (t (read.table ("Suwannee_0609-1331_wl.txt")) * 1000)
urb.im <- rast ("0913-1248_ref.dat")
urb.wl <- as.vector (t (read.table ("0913-1248_wl.txt")) * 1000)


im <- urb.im / 100 ## or veg.im or urb.im, /100 to scale reflectance in %
im.wl <- urb.wl ## or veg.wl or urb.wl

plotRGB (im, r=51, g=34, b=13, stretch="lin") ## plot a color composite of band
## 51(red), 34 (green), and 13 (blue)

#Q1.6: Use your newly gained knowledge on spectral properties of the surface to select three bands that
#enhance the main differences of the materials that you expect in your image. Which bands do you select?
#Why?




testspec <- click (im, n=1, show=FALSE); x11(); plot (im.wl, testspec, type="l",
xlab="Wavelength / nm", ylab="Reflectance / %") ## click in
## the image after running this code.





#Q1.7: Are you already familiar with the shape of some spectra? Can you identify some materials present in
#the image?